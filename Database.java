package dbconn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;


public class Database {
	 private Connection connect = null;
	 private Statement statement = null;
	 private PreparedStatement preparedStatement = null;
	 private ResultSet resultSet = null;

	 public void readDataBase() throws Exception 
	 {
		 Class.forName("com.mysql.jdbc.Driver");
		 connect = DriverManager
		          .getConnection("jdbc:mysql://localhost/loc-in?"
		              + "user=root&password=");
		 statement = connect.createStatement();
	 }
	 public void addUser(String name, String gender, String password, String userName, String Mail, String phone, String Type) throws SQLException
	 {
		preparedStatement = connect.prepareStatement("insert into user values ( ?, ?, ?, ? , ?, ?,?,?,default)");
		preparedStatement.setString(1, name);
		preparedStatement.setString(2, gender);
		preparedStatement.setString(3, password);
		preparedStatement.setString(4, Mail);
		preparedStatement.setString(5, phone);
		preparedStatement.setString(6, userName);
		preparedStatement.setString(7, Type);
		preparedStatement.setString(8, "1234567");
        preparedStatement.executeUpdate();
	 }
	 public int searchUser(String userName, String passWord) throws SQLException
	 {
		 resultSet = statement.executeQuery("select userNum  from user where userName= '"+userName+"' AND password = '"+passWord+"'");
		 int userNum = -1;
		 while (resultSet.next()) {
			  userNum=resultSet.getInt("userNum");
		 }
		 return userNum;
	 }
	 public int searchUser(String name) throws SQLException
	 {
		 resultSet = statement.executeQuery("select userNum  from user where userName= '"+name+"'");
		 int userNum = -1;
		 while (resultSet.next()) {
			  userNum=resultSet.getInt("userNum");
		 }
		 return userNum;
	 }
     public void addPlace(String placeName, float lang, float lat,String[] tastes) throws SQLException
	 {
		    preparedStatement = connect.prepareStatement("insert into place values ( ?, ?, ?,  default, ?, ?)");
			preparedStatement.setString(1, placeName);
			preparedStatement.setFloat(2, lang);
			preparedStatement.setFloat(3, lat);
			preparedStatement.setFloat(4, 0);
			preparedStatement.setInt(5, 0);
	        preparedStatement.executeUpdate(); 
	      
	        resultSet = statement.executeQuery("select placeNum  from place where placeName= '"+placeName+"' ");
			 int placeNum = -1;
			 while (resultSet.next()) {
				  placeNum=resultSet.getInt("placeNum");
			 }
			
	        for(int i=0;i<tastes.length;i++)
	        {
	        	addplacetaste( placeNum, tastes[i]);
	        }
	 }
	 public int searchPlace(String placeName) throws SQLException
	 {
		 resultSet = statement.executeQuery("select placeNum  from place where placeName= '"+placeName+"'");
		 int placeNum= -1;
		 while (resultSet.next()) {
			  placeNum=resultSet.getInt("placeNum");
		 }
		 return placeNum;
	 }
     public void addplacetaste(int placeNum,String taste) throws SQLException
     {
    	 preparedStatement = connect.prepareStatement("insert into placetastes values ( ?, ?)"); 
    	 preparedStatement.setInt(1, placeNum);
    	 preparedStatement.setString(2, taste);
    	  preparedStatement.executeUpdate();
     }
	 public void addfriendReq(int userNum1, int userNum2) throws SQLException
	 {
		preparedStatement = connect.prepareStatement("insert into friendrequest values ( ?, ?)");
	    preparedStatement.setInt(1, userNum1);
		preparedStatement.setInt(2, userNum2);
		preparedStatement.executeUpdate();
	 } 
	 public String getuserType(String name) throws SQLException
	 {
		 resultSet = statement.executeQuery("select AccType  from user where userName= '"+name+"'");
		 String Type=new String();
		 while (resultSet.next()) {
			  Type=resultSet.getString("userNum");
		 }
		 
		 return Type;
	 }
	 
	 
	 public static void main(String [] args) throws Exception
	 {
		 Database s=new Database();
		 s.readDataBase();
		// s.searchUser("AhmedSerag", "123456");
		 s.addUser("4", "4", "4", "a", "Naser@hamed4", "44476543","P4");
		 //String [] v=new String[2];
		 //v[0]="burger";
		 //v[1]="faheta";
		 //s.addPlace("shabrawy", 14, 12,v);
		// s.addfriendReq(1, 2);
		//s.addplacetaste(1,"shawerma");
	 }
}
